import java.awt.*;														//import packages
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Atm extends JFrame{										//inheritance
    boolean operation = false;											//initializing
    String input = "";															
    JPanel buttonPanel = new JPanel();
    JPasswordField jPass = new JPasswordField(4);
    String dispInput = "";
    int btnsize = 10;
    int attempts = 3;
    int correctPin = 1234;
    public Atm() {														//Constructor
    	super("ATM PIN AUTHENTICATION");
        add(jPass, BorderLayout.CENTER);
        jPass.setEditable(false);
        generateButton();
        buttonPanel.setPreferredSize(new Dimension(100, 400)); 
        add(buttonPanel, BorderLayout.SOUTH);
        setVisible(true);
        pack();
    }
    
    private void generateButton() {										//generate buttons
        // TODO Auto-generated method stub
        buttonPanel.setLayout(new GridLayout(4, 3));
        for (int i = 1; i < btnsize; i++) {								//generate number buttons except 0
            JButton jbtNumber = new JButton(i + "");
            jbtNumber.addActionListener(new buttonAction());
            buttonPanel.add(jbtNumber);
        }

        JButton jbtClear = new JButton("C");							//generate clear button
        jbtClear.addActionListener(new ClearAction());
        buttonPanel.add(jbtClear);
        
        JButton jbtNumber = new JButton("0");							//generate custom location for number 0
        jbtNumber.addActionListener(new buttonAction());
        buttonPanel.add(jbtNumber);
        
        JButton jbtEqual = new JButton("Enter");						//generate enter button
        jbtEqual.addActionListener(new EnterAction());
        buttonPanel.add(jbtEqual);
        
    }
    
    public class ClearAction implements ActionListener {				//action listener for clear button
        @Override
        public void actionPerformed(ActionEvent arg0) {
            // TODO Auto-generated method stub
            jPass.setText("");
            input = "";
            dispInput = "";
        }
    }
    
    public class EnterAction implements ActionListener {				//action listener for enter button
        @Override
        public void actionPerformed(ActionEvent arg0) {
            // TODO Auto-generated method stub
            jPass.setText("");
            //int pin = Integer.parseInt(input);
    		dispInput = "";

    		if (attempts>=0){											//check for number of attempts
            	validate();
            	}
            else{
            	JOptionPane.showMessageDialog(null, "You have reached the maximum number of attempts."
            			+ "  Please contact your administrator for assistance.");
            }															//msg for max attempts reached
        }
        void validate(){												//compare the input numbers to actual pin
        	int pin = Integer.parseInt(input);
        	
        	if (pin == correctPin){										//correct pin result msg
        		JOptionPane.showMessageDialog(null, "WELCOME!");
        		input = "";
        		dispInput = "";
        	}
        	else {																		//msg and result for incorrect pin
        		input = "";
        		dispInput = "";
            	if (attempts == 0){											
            		JOptionPane.showMessageDialog(null, "You have no more attempts remaining. Please Contact your administrator for assistance.");	
            		attempts = attempts - 1;											//making sure pin number is less than 5 numbers
            	}
            	else {
            		JOptionPane.showMessageDialog(null, "That is an incorrect PIN Please try again."
        				+ "  You have " + (attempts) + " attempts remaining.");				//Wrong Pin msg
            		attempts = attempts - 1;
            	}
        	}
        }
    }
   
    public class buttonAction implements ActionListener {									//action listener for the number buttons
        public void actionPerformed(ActionEvent e) {
            if (!operation) {
                setNumber(e);
            } else {
                input = "";
                operation = false;
                setNumber(e);
            }
            jPass.setText(dispInput);
        }
    }
    
    
    
    public void setNumber(ActionEvent e) {													//method for setting the number to string
        switch (e.getActionCommand()) {														
        case "1":
            input += 1;
            dispInput += "*";
            break;
        case "2":
            input += 2;
            dispInput += "*";
            break;
        case "3":
            input += 3;
            dispInput += "*";
            break;
        case "4":
            input += 4;
            dispInput += "*";
            break;
        case "5":
            input += 5;
            dispInput += "*";
            break;
        case "6":
            input += 6;
            dispInput += "*";
            break;
        case "7":
            input += 7;
            dispInput += "*";
            break;
        case "8":
            input += 8;
            dispInput += "*";
            break;
        case "9":
            input += 9;
            dispInput += "*";
            break;
        case "0":
            input += 0;
            dispInput += "*";
            break;
        }
    }
}
